import React from 'react';
import { mainOffice } from '../Offices/allOffices.js';
import ReactSwipe from 'react-swipe';

import '../Styles/header.css';
import logo from '../images/logo.jpg';
import photo1 from '../images/2.jpg';
import photo2 from '../images/3.jpg';
import photo3 from '../images/4.jpg';
import photo4 from '../images/6.jpg';
import photo5 from '../images/7.jpg';

export default class Header extends React.Component{

	render(){
		return(
			<header className="header">
				<div className="header__info">
					<div>
						<img src={logo} alt="Логотип" title="Логотип"/>
					</div>
					<div>
						<p>
						   299008, Севастополь,<br/>
						   {mainOffice.address}<br/>
						   <span>Время работы:</span><br/>
						   {mainOffice.workDays}<br/>
						   Забор биоматериала: {mainOffice.samplingBioMaterialTime}<br/>
						   Выдача результатов: {mainOffice.resultsTime}<br/>
						   Тел.: {mainOffice.phone}<br/>
						   email: {mainOffice.email}
						</p>
					</div>
				</div>
				<div className="header__slideShow">
					<Carousel />
				</div>
			</header>
		)
	}
}

function Carousel(){
	return(
		<ReactSwipe className="carousel" swipeOptions={{continuous: true, auto: 5000, speed: 700} }>
            <div><img src={photo1} alt=""/></div>
            <div><img src={photo2} alt=""/></div>
            <div><img src={photo3} alt=""/></div>
            <div><img src={photo4} alt=""/></div>
            <div><img src={photo5} alt=""/></div>
        </ReactSwipe>
	)
}
